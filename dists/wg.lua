#!/usr/bin/env lua
-- wg - A static website generator in Fennel
-- Author: Jesse Laprade (m455)
-- License: AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)
-- Source: https://git.m455.casa/m455/wg
local ffs
package.preload["ffs"] = package.preload["ffs"] or function(...)
  local ffs = {}
  ffs["shell->sequence"] = function(command)
    local file_handle = io.popen(command, "r")
    local function close_handlers_0_(ok_0_, ...)
      file_handle:close()
      if ok_0_ then
        return ...
      else
        return error(..., 0)
      end
    end
    local function _0_()
      local tbl_0_ = {}
      for i in file_handle:lines() do
        tbl_0_[(#tbl_0_ + 1)] = i
      end
      return tbl_0_
    end
    return close_handlers_0_(xpcall(_0_, (package.loaded.fennel or debug).traceback))
  end
  ffs["directory-exists?"] = function(directory)
    local command = string.format("test -d %s && echo exists", directory)
    if (ffs["shell->sequence"](command))[1] then
      return true
    else
      return false
    end
  end
  ffs["directory-create"] = function(dir)
    return os.execute(string.format("mkdir -p %s", dir))
  end
  ffs["directory-contents"] = function(dir)
    return ffs["shell->sequence"](string.format("ls %s", dir))
  end
  ffs["file-exists?"] = function(file)
    local command = string.format("test -f %s && echo exists", file)
    if (ffs["shell->sequence"](command))[1] then
      return true
    else
      return false
    end
  end
  ffs["file-create"] = function(file)
    return io.close(io.open(file, "w"))
  end
  ffs["file-write"] = function(file, data, mode)
    local file_out = io.open(file, mode)
    local function close_handlers_0_(ok_0_, ...)
      file_out:close()
      if ok_0_ then
        return ...
      else
        return error(..., 0)
      end
    end
    local function _0_()
      return file_out:write(data)
    end
    return close_handlers_0_(xpcall(_0_, (package.loaded.fennel or debug).traceback))
  end
  ffs["file->lines"] = function(file)
    local tbl_0_ = {}
    for line in io.lines(file) do
      tbl_0_[(#tbl_0_ + 1)] = line
    end
    return tbl_0_
  end
  ffs["path-exists?"] = function(path)
    return (ffs["file-exists?"](path) or ffs["directory-exists?"](path))
  end
  ffs["path-copy"] = function(source, dest)
    return os.execute(string.format("cp -r %s %s", source, dest))
  end
  ffs["path-delete"] = function(path)
    return os.execute(string.format("rm -rf %s", path))
  end
  ffs["paths-missing"] = function(mode, required_paths)
    local func
    do
      local _0_0 = mode
      if (_0_0 == "files") then
        func = ffs["file-exists?"]
      elseif (_0_0 == "directories") then
        func = ffs["directory-exists?"]
      else
      func = nil
      end
    end
    local tbl_0_ = {}
    for _, path in ipairs(required_paths) do
      local _1_
      if not func(path) then
        _1_ = path
      else
      _1_ = nil
      end
      tbl_0_[(#tbl_0_ + 1)] = _1_
    end
    return tbl_0_
  end
  return ffs
end
ffs = require("ffs")
local directories_required = {"build", "layout", "copy", "convert"}
local files_required = {footer = "layout/footer.md", header = "layout/header.md", index = "convert/index.md", style = "copy/style.css"}
local contents_index = ("This is some default text found in `convert/index.md`.\n\n" .. "If you want to change the look of your website, you can\n" .. "find the CSS styles in `copy/style.css`.\n\n" .. "To change the header and footer, check out the `layout/` directory.")
local contents_style = ("body {\n" .. "  background-color: pink;\n" .. "}\n\n" .. "code {\n" .. "  background-color: black;\n" .. "  color: pink;\n" .. "}")
local contents_header = ("---\n" .. "title: 'The title of your website'\n" .. "lang: en\n" .. "header-includes:\n" .. "  <meta name=\"author\" content=\"Your name\"/>\n" .. "  <meta name=\"description\" content=\"Some description\"/>\n" .. "  <meta name=\"keywords\" content=\"programming, documentation\"/>\n" .. "---" .. "\n" .. "<nav>[Home](/) - [Another home link](/index.html)</nav>\n" .. "<hr/>\n" .. "<main>\n")
local contents_footer = ("</main>\n" .. "<hr/>\n" .. "<footer>This website was built with [wg](https://git.m455.casa/m455/wg)</footer>\n")
local function print_format(str, ...)
  return print(string.format(str, ...))
end
local function print_missing_files()
  print("Error: wg has not been initialized or there are missing files.")
  print("If wg has been initialized, try running the following command:")
  print("  wg repair")
  print("If wg has not been initialized, try running the following command:")
  print("  wg init")
  print("For more help, type the following command:")
  return print("  wg help")
end
local function required_paths_missing_3f()
  return ((#ffs["paths-missing"]("directories", directories_required) > 0) or (#ffs["paths-missing"]("files", files_required) > 0))
end
local function build_directory_has_contents_3f()
  return (#ffs["directory-contents"]("build") > 0)
end
local function init_2fstart()
  for _, dir in ipairs(directories_required) do
    print_format("Creating '%s/'...", dir)
    ffs["directory-create"](dir)
  end
  for _, file in ipairs({files_required.index, files_required.style, files_required.header, files_required.footer}) do
    print_format("Creating '%s'...", file)
    ffs["file-create"](file)
  end
  ffs["file-write"](files_required.index, contents_index, "w")
  ffs["file-write"](files_required.style, contents_style, "w")
  ffs["file-write"](files_required.header, contents_header, "w")
  ffs["file-write"](files_required.footer, contents_footer, "w")
  return print("Initialization complete!")
end
local function init_2fread_input()
  local input = io.read(1)
  if (input == "y") then
    return init_2fstart()
  else
    return print("Cancelled the creation of the required directories and files.")
  end
end
local function init_2fprompt()
  print("The following directories and files will be created:")
  for _, dir in ipairs(directories_required) do
    print_format("  %s/", dir)
  end
  for _, file in pairs(files_required) do
    print_format("  %s", file)
  end
  print("This will overwrite any files with the same names as the files above.")
  print("Are you sure you want to do this? (y/n)")
  io.write("> ")
  return init_2fread_input()
end
local function init()
  if required_paths_missing_3f() then
    return init_2fprompt()
  else
    return print("The required directories and files already exist.")
  end
end
local function serve()
  if required_paths_missing_3f() then
    return print_missing_files()
  else
    if build_directory_has_contents_3f() then
      return os.execute("python3 -m http.server 8000 --directory build/")
    else
      print("Error: 'build/' directory has no contents.")
      print("Try running the following command first:")
      return print("  wg build")
    end
  end
end
local function clean_2fstart()
  print("Deleting contents of 'build/' directory...")
  ffs["path-delete"](("build" .. "/*"))
  return print("Cleaning complete!")
end
local function clean_2fprompt()
  print("Cleaning will delete everything in the 'build/' directory.")
  print("Do you want to continue? (y/n)")
  io.write("> ")
  local input = io.read(1)
  if (input == "y") then
    return clean_2fstart()
  else
    return print("Cancelled the deletion of the 'build/' directory's contents.")
  end
end
local function clean()
  if required_paths_missing_3f() then
    return print_missing_files()
  else
    if build_directory_has_contents_3f() then
      return clean_2fprompt()
    else
      return print("'build/' directory empty. Nothing to clean.")
    end
  end
end
local function markdown__3ehtml(source_file, destination_file)
  return os.execute(string.format("pandoc -s -c /style.css %s %s %s -o %s --shift-heading-level-by=1", files_required.header, source_file, files_required.footer, destination_file))
end
local function build_2fconvert(dir)
  for _, path in ipairs(ffs["directory-contents"](dir)) do
    local source_path = (dir .. "/" .. path)
    if (ffs["file-exists?"](source_path) and string.match(source_path, ".md")) then
      local destination_dir = string.gsub(string.gsub(source_path, "(.*/).*.md", "%1"), "^convert/", "build/")
      local destination_file = string.gsub(string.gsub(source_path, ".md", ".html"), "^convert/", "build/")
      if not ffs["directory-exists?"](destination_dir) then
        ffs["directory-create"](destination_dir)
      end
      markdown__3ehtml(source_path, destination_file)
    else
      if ffs["directory-exists?"](source_path) then
        build_2fconvert(source_path)
      end
    end
  end
  return nil
end
local function build()
  if required_paths_missing_3f() then
    return print_missing_files()
  else
    if (#ffs["directory-contents"]("copy") == 0) then
      print("No directories or files found in the 'copy/' directory. Skipping...")
    else
      print("Copying files in 'copy/' directory...")
      ffs["path-copy"](("copy" .. "/*"), "build")
      print("Copying complete!")
    end
    if (#ffs["directory-contents"]("convert") == 0) then
      return print("No directories or files found in the 'convert/' directory. Skipping...")
    else
      print("Converting files in 'convert/' directory...")
      build_2fconvert("convert")
      return print("Conversion complete!")
    end
  end
end
local function repair_2fstart()
  for _, dir in ipairs(directories_required) do
    if not ffs["directory-exists?"](dir) then
      print_format("Creating '%s/'...", dir)
      ffs["directory-create"](dir)
    end
  end
  for _, file in ipairs({files_required.index, files_required.style, files_required.header, files_required.footer}) do
    if not ffs["file-exists?"](file) then
      print_format("Creating '%s'...", file)
      ffs["file-create"](file)
      local make_file
      local function _0_(file0, contents)
        return ffs["file-write"](file0, contents, "w")
      end
      make_file = _0_
      local _1_0 = file
      if (_1_0 == files_required.index) then
        make_file(files_required.index, contents_index)
      elseif (_1_0 == files_required.style) then
        make_file(files_required.style, contents_style)
      elseif (_1_0 == files_required.header) then
        make_file(files_required.header, contents_header)
      elseif (_1_0 == files_required.footer) then
        make_file(files_required.footer, contents_footer)
      end
    end
  end
  return print("Repair complete!")
end
local function repair_2fread_input()
  local input = io.read(1)
  if (input == "y") then
    return repair_2fstart()
  else
    return print("Cancelled the repairs.")
  end
end
local function repair_2fprompt()
  print("The following files or directories are missing:")
  for _, dir in ipairs(directories_required) do
    if not ffs["directory-exists?"](dir) then
      print_format("  %s/", dir)
    end
  end
  for _, file in pairs(files_required) do
    if not ffs["file-exists?"](file) then
      print_format("  %s", file)
    end
  end
  print("Do you want to create these files? (y/n)")
  io.write("> ")
  return repair_2fread_input()
end
local function repair()
  if required_paths_missing_3f() then
    return repair_2fprompt()
  else
    return print("The required directories and files already exist.")
  end
end
local function help()
  return print(("wg\n" .. "    A static website generator written in Fennel.\n" .. "\n" .. "Author\n" .. "    Jesse Laprade (m455)\n" .. "\n" .. "License\n" .. "    AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)\n" .. "\n" .. "Source\n" .. "    https://git.m455.casa/m455/wg\n" .. "\n" .. "Commands\n" .. "    init\n" .. "        Creates required directories and files in the current directory.\n" .. "\n" .. "    build\n" .. "        Recursively copies directories and files from the 'copy/'\n" .. "        directory into the 'build/' directory, preserving the directory\n" .. "        structure of the 'copy/' directory.\n" .. "\n" .. "        Recursively converts Markdown files in the 'convert/' directory\n" .. "        to HTML files in the 'build/' directory, preserving the\n" .. "        directory structure of the 'convert/' directory.\n" .. "\n" .. "    serve\n" .. "        Serves files in the 'build/' directory on port 8000, allowing\n" .. "        you to see how your website will look locally before it goes\n" .. "        live.\n" .. "\n" .. "    clean\n" .. "        Deletes all contents of the 'build/' directory.\n" .. "\n" .. "    repair\n" .. "        Looks for and creates missing files or directories.\n" .. "\n" .. "    help\n" .. "        Displays this help message.\n" .. "\n" .. "Example usage\n" .. "    wg init\n" .. "    wg build\n" .. "    wg serve\n" .. "    wg clean\n" .. "    wg help\n"))
end
local function main(arg_tbl)
  local _0_0 = arg_tbl
  if ((type(_0_0) == "table") and ((_0_0)[1] == "init") and ((_0_0)[2] == nil)) then
    return init()
  elseif ((type(_0_0) == "table") and ((_0_0)[1] == "build") and ((_0_0)[2] == nil)) then
    return build()
  elseif ((type(_0_0) == "table") and ((_0_0)[1] == "serve") and ((_0_0)[2] == nil)) then
    return serve()
  elseif ((type(_0_0) == "table") and ((_0_0)[1] == "clean") and ((_0_0)[2] == nil)) then
    return clean()
  elseif ((type(_0_0) == "table") and ((_0_0)[1] == "repair") and ((_0_0)[2] == nil)) then
    return repair()
  elseif ((type(_0_0) == "table") and ((_0_0)[1] == "help") and ((_0_0)[2] == nil)) then
    return help()
  else
    local _ = _0_0
    print("For more information, type the following command:")
    return print("  wg help")
  end
end
return main(arg)
