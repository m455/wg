;; ffs - Fennel file system utilities
;; Author: Jesse Laprade (m455)
;; License: AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)
;; Source: https://git.m455.casa/m455/ffs

(local ffs {})

(fn ffs.shell->sequence [command]
  (with-open [file-handle (io.popen command :r)]
    (icollect [i (file-handle:lines)] i)))

;; The (if ...) here makes sure a "/" is appended to the path
(fn ffs.directory-exists? [directory]
  (let [command (string.format "test -d %s && echo exists" directory)]
    (if (. (ffs.shell->sequence command) 1)
        true ;; This prevents "exists" from being returned
        false)))

(fn ffs.directory-create [dir]
  (os.execute (string.format "mkdir -p %s" dir)))

(fn ffs.directory-contents [dir]
  (ffs.shell->sequence (string.format "ls %s" dir)))

(fn ffs.file-exists? [file]
  (let [command (string.format "test -f %s && echo exists" file)]
    (if (. (ffs.shell->sequence command) 1)
        true ;; This prevents "exists" from being returned
        false)))

(fn ffs.file-create [file]
  (io.close (io.open file :w)))

(fn ffs.file-write [file data mode]
  (with-open [file-out (io.open file mode)]
    (file-out:write data)))

(fn ffs.file->lines [file]
  (icollect [line (io.lines file)] line))

(fn ffs.path-exists? [path]
  (or (ffs.file-exists? path)
      (ffs.directory-exists? path)))

(fn ffs.path-copy [source dest]
  (os.execute (string.format "cp -r %s %s" source dest)))

(fn ffs.path-delete [path]
  (os.execute (string.format "rm -rf %s" path)))

(fn ffs.paths-missing [mode required-paths]
  (let [func (match mode
               :files ffs.file-exists?
               :directories ffs.directory-exists?)]
    (icollect [_ path (ipairs required-paths)]
      (when (not (func path))
        path))))

ffs
