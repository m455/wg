# wg

A static website generator written in Fennel.

## Table of Contents

<!-- vim-markdown-toc GFM -->

* [Document conventions](#document-conventions)
* [Requirements](#requirements)
    * [Requirements for installing and running wg](#requirements-for-installing-and-running-wg)
    * [Requirements for buildings wg from source](#requirements-for-buildings-wg-from-source)
* [Quick start](#quick-start)
* [Downloading wg](#downloading-wg)
    * [Downloading the source code](#downloading-the-source-code)
        * [To download the source code](#to-download-the-source-code)
* [Installing wg](#installing-wg)
    * [Installing wg globally](#installing-wg-globally)
        * [To install wg globally](#to-install-wg-globally)
    * [Installing wg locally](#installing-wg-locally)
        * [To install wg locally](#to-install-wg-locally)
    * [Installing wg to a custom location](#installing-wg-to-a-custom-location)
        * [To install wg to a custom location](#to-install-wg-to-a-custom-location)
* [Uninstalling wg](#uninstalling-wg)
    * [Uninstalling wg globally](#uninstalling-wg-globally)
        * [To uninstall wg globally](#to-uninstall-wg-globally)
    * [Uninstalling wg locally](#uninstalling-wg-locally)
        * [To uninstall wg locally](#to-uninstall-wg-locally)
    * [Uninstalling wg from a custom location](#uninstalling-wg-from-a-custom-location)
        * [To uninstall wg from a custom location](#to-uninstall-wg-from-a-custom-location)
* [Building wg from source](#building-wg-from-source)
    * [Building a Lua script](#building-a-lua-script)
        * [To build a Lua script](#to-build-a-lua-script)
* [Commands](#commands)
* [Usage](#usage)
* [Default files and directories explained](#default-files-and-directories-explained)
    * [The layout directory](#the-layout-directory)
        * [The header file](#the-header-file)
        * [The footer file](#the-footer-file)
    * [The convert directory](#the-convert-directory)
    * [The copy directory](#the-copy-directory)
    * [The build directory](#the-build-directory)

<!-- vim-markdown-toc -->

## Document conventions

* **Note**: Notes signify additional information.
* **Tip**: Tips signify an alternative procedure for completing a step.
* **Warning**: Warnings signify that damage, such as data loss, may occur.
* **Example**: Examples provide a reference of how a procedure would be performed in the real world.
* `Inline code`: Inline code signifies package names, filenames, or commands.
* `Code block`: Code blocks signify file contents.

## Requirements

This section describes two different sets of requirements:

* [Requirements for installing and running wg](#requirements-for-installing-and-running-wg)
* [Requirements for buildings wg from source](#requirements-for-buildings-wg-from-source)

### Requirements for installing and running wg

* Python3
* Lua5.3 or above

### Requirements for buildings wg from source

* [Fennel](https://fennel-lang.org/)

## Quick start

1. Download the `wg.lua` file using one of the following commands:
     * `curl https://git.m455.casa/wg/plain/dists/wg.lua -o wg`
     * `wget https://git.m455.casa/wg/plain/dists/wg.lua -O wg`
2. Move the `wg` to a directory on your `$PATH`
3. Run `wg help`

## Downloading wg

This section will guide you through downloading wg in your current directory.

### Downloading the source code

The source code for wg is publicly available in a Git repository. Source code is
useful if you want inspect a program's behaviour before using it. The source
code for wg contains a `wg.lua` file, which is the file you will need for
installing wg on your system.

#### To download the source code

1. `git clone git://git.m455.casa/wg`

## Installing wg

This section will guide you through downloading and installing wg either
globally or locally.

This section consists of the following subsections:

* [Installing wg globally](#installing-wg-globally)
* [Installing wg locally](#installing-wg-locally)
* [Installing wg to a custom location](#installing-wg-to-a-custom-location)

### Installing wg globally

This section will guide you through installing wg globally. This method
will install wg into `/usr/local/bin`, which requires you to have root
access to your machine.

**Tip**: If you don't have root access to your machine, check out the
[Installing wg locally](#installing-wg-locally) section.

#### To install wg globally

1. `cd wg`
2. `sudo make install`

### Installing wg locally

This section will guide you through installing wg locally. This method
will install wg into `~/.local/bin`.

#### To install wg locally

1. `cd wg`
2. `make install-local`

### Installing wg to a custom location

This section will guide you through installing wg to a custom location.

#### To install wg to a custom location

1. `cd wg`
2. `make DESTDIR=~/path/to/custom/location install`

**Note**: If you choose to install wg to a custom location, you will
need to remember where you installed wg if you decide to uninstall it
later.

## Uninstalling wg

This section will guide you through uninstalling wg either globally or
locally.

This section consists of the following subsections:

* [Uninstalling wg globally](#uninstalling-wg-globally)
* [Uninstalling wg locally](#uninstalling-wg-locally)
* [Uninstalling wg from a custom location](#uninstalling-wg-from-a-custom-location)

### Uninstalling wg globally

This section will guide you through uninstalling wg globally. This
method will remove wg from `/usr/local/bin`, which requires you to have
root access to your machine.

#### To uninstall wg globally

1. `cd wg`
1. `make uninstall`

### Uninstalling wg locally

This section will guide you through uninstalling wg locally. This method
will remove wg from `~/.local/bin`.

#### To uninstall wg locally

1. `cd wg`
1. `make uninstall-local`

### Uninstalling wg from a custom location

This section will guide you through uninstalling wg from a custom
location.

#### To uninstall wg from a custom location

1. `cd wg`
2. `make DESTDIR=~/path/to/custom/location uninstall`

## Building wg from source

This section will guide you through compiling the files that make up wg
into a single Lua script.

### Building a Lua script

Fennel allows you to build a single Lua script from several source files. The
Makefile in this repository will create a `wg.lua` script in the root directory
of the repository.

#### To build a Lua script

1. `make compile`

## Commands

* `init` - Creates required directories and files in the current
  directory.
* `build`
     * Recursively copies directories and files from the `copy/`
       directory into the `build/` directory.
     * Recursively converts Markdown files in the `convert/` directory
       to HTML files in the `build/` directory.
* `serve` - Serves files in the 'build/' directory on port 8000,
  allowing you to see how your website will look locally before it goes
  live.
* `clean` - Deletes all contents of the 'build/' directory.
* `Repair` - Looks for and creates missing files or directories.
* `help` - Displays the help message.

## Usage

* `wg init`
* `wg build`
* `wg serve`
* `wg clean`
* `wg repair`
* `wg help`

## Default files and directories explained

This section explains each of the default files and directories that are
created after running `wg init`.

### The layout directory

* The `layout` directory must contain a `header.md` file and `footer.md`
  file.

#### The header file

The contents in the `header.md` file will be used as a header for any
HTML files that were generated from the `convert` directory.

#### The footer file

The contents in the `footer.md` file will be used as a footer for any
HTML files that were generated from the `convert` directory.

### The convert directory

* The `convert` directory must contain an `index.md` file.
* The `index.md` can be empty.
* The `index.md` file will be used as the landing page.
* The `convert` directory should only contain directories, and Markdown
  files ending in `.md`.
* The directory structure in the `convert` directory will be mimicked in
  the `build` directory.
* Empty directories will not be copied to the `build` directory.
* Markdown files will be converted to HTML and moved into the `build` directory.
* Markdown files contain a header and footer, which can both be customized by editing
  `layout/header.md` and `layout/footer.md`.
`df`
### The copy directory

* The `copy` directory must contain a `style.css` file.
* The `style.css` can be empty.
* The `style.css` will be linked to from any HTML files that were
  generated from the `convert` directory.
* The `copy` directory can contain any files or directories.
* The directory structure in the `copy` directory will be mimicked in
  the `build` directory.
* Empty directories will not be copied to the `build` directory.

### The build directory

* The `build` directory is used by the `build` command.
* The `build` directory contains:
    * The generated website.
    * HTML files that were either generated from Markdown files from the
      `convert` directory, or copied from the `copy` directory.
    * The directory structures from the `convert` and `copy` directories, and
      the `style.css` file from the `copy` directory.

**Note**: This is the directory you will want to use as your website.
