(local ffs (require :ffs))

(local directories-required [:build :layout :copy :convert])

(local files-required {:index  "convert/index.md"
                       :style  "copy/style.css"
                       :header "layout/header.md"
                       :footer "layout/footer.md"})

(local contents-index
  (.. "This is some default text found in `convert/index.md`.\n\n"
      "If you want to change the look of your website, you can\n"
      "find the CSS styles in `copy/style.css`.\n\n"
      "To change the header and footer, check out the `layout/` directory."))

(local contents-style
  (.. "body {\n"
      "  background-color: pink;\n"
      "}\n\n"
      "code {\n"
      "  background-color: black;\n"
      "  color: pink;\n"
      "}"))

(local contents-header
  (.. "---\n"
      "title: 'The title of your website'\n"
      "lang: en\n"
      "header-includes:\n"
      "  <meta name=\"author\" content=\"Your name\"/>\n"
      "  <meta name=\"description\" content=\"Some description\"/>\n"
      "  <meta name=\"keywords\" content=\"programming, documentation\"/>\n"
      "---"
      "\n"
      "<nav>[Home](/) - [Another home link](/index.html)</nav>\n"
      "<hr/>\n"
      "<main>\n"))

(local contents-footer
  (.. "</main>\n"
      "<hr/>\n"
      "<footer>This website was built with [wg](https://git.m455.casa/m455/wg)</footer>\n"))

(fn print-format [str ...]
  (print (string.format str ...)))

(fn print-missing-files []
  (print "Error: wg has not been initialized or there are missing files.")
  (print "If wg has been initialized, try running the following command:")
  (print "  wg repair")
  (print "If wg has not been initialized, try running the following command:")
  (print "  wg init")
  (print "For more help, type the following command:")
  (print "  wg help"))

;; ---------------------------------------
;; Path utils
;; ---------------------------------------
(fn required-paths-missing? []
  (or (> (length (ffs.paths-missing :directories directories-required)) 0)
      (> (length (ffs.paths-missing :files files-required)) 0)))

(fn build-directory-has-contents? []
  (> (length (ffs.directory-contents :build)) 0))

;; ---------------------------------------
;; init
;; ---------------------------------------
(fn init/start []
  ;; Create required directories
  (each [_ dir (ipairs directories-required)]
    (print-format "Creating '%s/'..." dir)
    (ffs.directory-create dir))
  ;; Create and populate required files
  ;; Create required files
  (each [_ file (ipairs [files-required.index
                         files-required.style
                         files-required.header
                         files-required.footer])]
    (print-format "Creating '%s'..." file)
    (ffs.file-create file))
  ;; Populate required files
  (ffs.file-write files-required.index contents-index :w)
  (ffs.file-write files-required.style contents-style :w)
  (ffs.file-write files-required.header contents-header :w)
  (ffs.file-write files-required.footer contents-footer :w)
  (print "Initialization complete!"))

(fn init/read-input []
  (let [input (io.read 1)]
    (if (= input :y)
      (init/start)
      (print "Cancelled the creation of the required directories and files."))))

(fn init/prompt []
  (print "The following directories and files will be created:")
  (each [_ dir (ipairs directories-required)]
    (print-format "  %s/" dir))
  (each [_ file (pairs files-required)]
    (print-format "  %s" file))
  (print "This will overwrite any files with the same names as the files above.")
  (print "Are you sure you want to do this? (y/n)")
  (io.write "> ")
  (init/read-input))

(fn init []
  (if (required-paths-missing?)
    (init/prompt)
    (print "The required directories and files already exist.")))

;; ---------------------------------------
;; serve
;; ---------------------------------------
(fn serve []
  (if (required-paths-missing?)
    (print-missing-files)
    (if (build-directory-has-contents?)
      (os.execute "python3 -m http.server 8000 --directory build/")
      (do (print "Error: 'build/' directory has no contents.")
        (print "Try running the following command first:")
        (print "  wg build")))))

;; ---------------------------------------
;; clean
;; ---------------------------------------
(fn clean/start []
  (print "Deleting contents of 'build/' directory...")
  (ffs.path-delete (.. :build "/*"))
  (print "Cleaning complete!"))

(fn clean/prompt []
  (print "Cleaning will delete everything in the 'build/' directory.")
  (print "Do you want to continue? (y/n)")
  (io.write "> ")
  (let [input (io.read 1)]
    (if (= input :y)
      (clean/start)
      (print "Cancelled the deletion of the 'build/' directory's contents."))))

(fn clean []
  (if (required-paths-missing?)
    (print-missing-files)
    (if (build-directory-has-contents?)
      (clean/prompt)
      (print "'build/' directory empty. Nothing to clean."))))

;; ---------------------------------------
;; build
;; ---------------------------------------
;; Note: source and destination should both be full paths starting from
;;       where wg.fnl is ran
(fn markdown->html [source-file destination-file]
  (os.execute
    (string.format "pandoc -s -c /style.css %s %s %s -o %s --shift-heading-level-by=1"
                   (. files-required :header)
                   source-file
                   (. files-required :footer)
                   destination-file)))

(fn build/convert [dir]
  (each [_ path (ipairs (ffs.directory-contents dir))]
    (let [source-path (.. dir "/" path)]
      (if (and (ffs.file-exists? source-path)
               (string.match source-path ".md"))
        (let [destination-dir (-> source-path
                                  (string.gsub "(.*/).*.md" "%1")     ;; => convert/some/path/
                                  (string.gsub "^convert/" "build/")) ;; => build/some/path/
              destination-file (-> source-path
                                   (string.gsub ".md" ".html")          ;; => convert/some/path/file.html
                                   (string.gsub "^convert/" "build/"))] ;; => build/some/path/file.html
          (when (not (ffs.directory-exists? destination-dir))
            (ffs.directory-create destination-dir))
          (markdown->html source-path destination-file))
        (when (ffs.directory-exists? source-path)
          (build/convert source-path))))))

(fn build []
  (if (required-paths-missing?)
    (print-missing-files)
    (do ;; Copy paths
      (if (= (length (ffs.directory-contents :copy)) 0)
        (print "No directories or files found in the 'copy/' directory. Skipping...")
        (do (print "Copying files in 'copy/' directory...")
          (ffs.path-copy (.. :copy "/*") :build)
          (print "Copying complete!")))
      ;; Convert paths
      (if (= (length (ffs.directory-contents :convert)) 0)
        (print "No directories or files found in the 'convert/' directory. Skipping...")
        (do (print "Converting files in 'convert/' directory...")
          (build/convert :convert)
          (print "Conversion complete!"))))))

(fn repair/start []
  (each [_ dir (ipairs directories-required)]
    (when (not (ffs.directory-exists? dir))
      (print-format "Creating '%s/'..." dir)
      (ffs.directory-create dir)))
  (each [_ file (ipairs [files-required.index
                         files-required.style
                         files-required.header
                         files-required.footer])]
    (when (not (ffs.file-exists? file))
      (print-format "Creating '%s'..." file)
      (ffs.file-create file)
      (let [make-file (fn [file contents]
                        (ffs.file-write file contents :w))]
        (match file
          files-required.index  (make-file files-required.index contents-index)
          files-required.style  (make-file files-required.style contents-style)
          files-required.header (make-file files-required.header contents-header)
          files-required.footer (make-file files-required.footer contents-footer)))))
  (print "Repair complete!"))

(fn repair/read-input []
  (let [input (io.read 1)]
    (if (= input :y)
      (repair/start)
      (print "Cancelled the repairs."))))

(fn repair/prompt []
  (print "The following files or directories are missing:")
  (each [_ dir (ipairs directories-required)]
    (when (not (ffs.directory-exists? dir))
      (print-format "  %s/" dir)))
  (each [_ file (pairs files-required)]
    (when (not (ffs.file-exists? file))
      (print-format "  %s" file)))
  (print "Do you want to create these files? (y/n)")
  (io.write "> ")
  (repair/read-input))

(fn repair []
  (if (required-paths-missing?)
    (repair/prompt)
    (print "The required directories and files already exist.")))

(fn help []
  (print
    (.. "wg\n"
        "    A static website generator written in Fennel.\n"
        "\n"
        "Author\n"
        "    Jesse Laprade (m455)\n"
        "\n"
        "License\n"
        "    AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)\n"
        "\n"
        "Source\n"
        "    https://git.m455.casa/m455/wg\n"
        "\n"
        "Commands\n"
        "    init\n"
        "        Creates required directories and files in the current directory.\n"
        "\n"
        "    build\n"
        "        Recursively copies directories and files from the 'copy/'\n"
        "        directory into the 'build/' directory, preserving the directory\n"
        "        structure of the 'copy/' directory.\n"
        "\n"
        "        Recursively converts Markdown files in the 'convert/' directory\n"
        "        to HTML files in the 'build/' directory, preserving the\n"
        "        directory structure of the 'convert/' directory.\n"
        "\n"
        "    serve\n"
        "        Serves files in the 'build/' directory on port 8000, allowing\n"
        "        you to see how your website will look locally before it goes\n"
        "        live.\n"
        "\n"
        "    clean\n"
        "        Deletes all contents of the 'build/' directory.\n"
        "\n"
        "    repair\n"
        "        Looks for and creates missing files or directories.\n"
        "\n"
        "    help\n"
        "        Displays this help message.\n"
        "\n"
        "Example usage\n"
        "    wg init\n"
        "    wg build\n"
        "    wg serve\n"
        "    wg clean\n"
        "    wg help\n")))

;; ---------------------------------------
;; Arg parsing
;; ---------------------------------------
(fn main [arg-tbl]
  (match arg-tbl
    [:init nil]   (init)
    [:build nil]  (build)
    [:serve nil]  (serve)
    [:clean nil]  (clean)
    [:repair nil] (repair)
    [:help nil]   (help)
    _             (do (print "For help, type the following command:")
                    (print "  wg help"))))

(main arg)
