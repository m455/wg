SRCDIR = src
COMPILEDIR = dists
DESTDIR ?= /usr/local/bin
LOCALDIR = ~/.local/bin

.PHONY: help
help:
	@echo "Usage:"
	@echo "    make help            - Displays this help message."
	@echo "    make compile         - Compiles source into $(COMPILEDIR)/wg.lua."
	@echo "    make install         - Installs wg to $(DESTDIR)."
	@echo "    make install-local   - Installs wg to $(LOCALDIR)."
	@echo "    make uninstall       - Deletes wg from $(DESTDIR)."
	@echo "    make uninstall-local - Deletes wg from $(LOCALDIR)."

compile:
	@echo "#!/usr/bin/env lua" > $(COMPILEDIR)/wg.lua
	@echo "-- wg - A static website generator in Fennel" >> $(COMPILEDIR)/wg.lua
	@echo "-- Author: Jesse Laprade (m455)" >> $(COMPILEDIR)/wg.lua
	@echo "-- License: AGPL3 (https://www.gnu.org/licenses/agpl-3.0.en.html)" >> $(COMPILEDIR)/wg.lua
	@echo "-- Source: https://git.m455.casa/m455/wg" >> $(COMPILEDIR)/wg.lua
	@cd $(SRCDIR) && fennel --require-as-include --compile wg.fnl >> ../$(COMPILEDIR)/wg.lua
	@echo "Successfully compiled wg into $(COMPILEDIR)/wg.lua!"

install:
	@install -Dm755 ./$(COMPILEDIR)/wg.lua -D $(DESTDIR)/wg
	@echo "Successfully installed to $(DESTDIR)!"

install-local:
	@install -Dm755 ./$(COMPILEDIR)/wg.lua -D $(LOCALDIR)/wg
	@echo "Successfully installed to $(LOCALDIR)!"

uninstall:
	@rm $(DESTDIR)/wg
	@echo "Successfully uninstalled wg from $(DESTDIR)!"

uninstall-local:
	@rm $(LOCALDIR)/wg
	@echo "Successfully uninstalled wg from $(LOCALDIR)!"

